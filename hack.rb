APP_ROOT = File.join(File.dirname(__FILE__))

require 'rubygems'
require 'sinatra'
require 'haml'
require 'koala'

# facebook info
APP_ID = 246683162076815 # your app id
APP_CODE = '68237bb5b70480e25a1b12320782ac0b' # your app code
SITE_URL = 'http://apps.mcamac.com' # your app site url

set :root, APP_ROOT
enable :sessions

get '/' do
	haml :index
end

get '/test' do

	#haml :index
	if session['access_token']
		@message = 'You are logged in! <a href="/logout">Logout</a>'
		@fbname = 'brandon'
			# do some stuff with facebook here
			# for example:
			# @graph = Koala::Facebook::GraphAPI.new(session["access_token"])
			# publish to your wall (if you have the permissions)
			# @graph.put_wall_post("I'm posting from my new cool app!")
			# or publish to someone else (if you have the permissions too ;) )
			# @graph.put_wall_post("Checkout my new cool app!", {}, "someoneelse's id")
	else
		@message = '<a href="/login">Login</a>'
	end

	haml :test
end



get '/login' do
	# generate a new oauth object with your app data and your callback url
	session['oauth'] = Koala::Facebook::OAuth.new(APP_ID, APP_CODE, SITE_URL + ':9393/callback')
	# redirect to facebook to get your code
	redirect session['oauth'].url_for_oauth_code()
end

get '/logout' do
	session['oauth'] = nil
	session['access_token'] = nil
	redirect '/'
end

#method to handle the redirect from facebook back to you
get '/callback' do
	#get the access token from facebook with your code
	session['access_token'] = session['oauth'].get_access_token(params[:code])
	redirect '/'
end

get '/hi' do
	"Hello World!"
end
